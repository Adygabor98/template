/**
 * Import Zone
 */
const { ApolloServer } = require('apollo-server');
const typeDefs = require('./database/schema');
const resolvers = require('./database/resolvers');


/**
 * Creating the server variable
 */
const server = new ApolloServer({
    typeDefs, 
    resolvers,
    /* Create the context that is shared among all resolvers  */
    context : () => {
        const user = "George Adrian Gabor";

        return {
            user
        }
    }
});


/**
 * Running the server
 */
 server.listen().then(({url})=>{
     console.log(`Server running in ${url}`);
 })