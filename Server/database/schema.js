const { gql} = require('apollo-server');

/**
 * SCHEMA
 */

// Si en l'input es posa ! significa que 
// aquell input es obligatori sino es que pot ser
// opcional

const typeDefs = gql`
   type Query{
       obtenerCurso: String
   }
`;

module.exports = typeDefs;